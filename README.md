# Projects / `GitLab CI` templates

[![Software License](https://img.shields.io/badge/license-MIT-informational.svg?style=for-the-badge)](LICENSE)
[![semantic-release: angular](https://img.shields.io/badge/semantic--release-angular-e10079?logo=semantic-release&style=for-the-badge)](https://github.com/semantic-release/semantic-release)
[![Pipeline Status](https://img.shields.io/gitlab/pipeline-status/op_so/projects/gitlab-ci-templates?style=for-the-badge)](https://gitlab.com/op_so/projects/gitlab-ci-templates/pipelines)

A set of reusable `GitLab CI` templates:

* [`ansible.galaxy-publish.gitlab-ci.yml`](#ansible-galaxy): a template to publish playbooks to [`Ansible Galaxy`](https://galaxy.ansible.com/).
* [`lint.gitlab-ci.yml`](#lint): a template to lint different kind of files.
* [`lint.python-rye.gitlab-ci.yml`](#lint-python): a template to lint python files.
* [`image-factory.gitlab-ci.yml`](#image-factory): a template to automatically sign, update and publish Docker images.
* [`gitlab-release.gitlab-ci.yml`](#gitlab-release): a template to publish a release with [Semantic Release](https://github.com/semantic-release/semantic-release).
* [`python.rye-publish.gitlab-ci.yml`](#python-release-with-rye): a template to publish to [PyPI](https://pypi.org/) and `GitLab` pages.
* [`robotframework.gitlab-ci.yml`](#robot-framework): a template to lint [Robot Framework](https://robotframework.org/) tests.
* [`robotframework-pages.gitlab-ci.yml`](#robot-framework-pages): a template to publish [Robot Framework](https://robotframework.org/) results on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

## `Ansible Galaxy`

### `Ansible Galaxy` - details

One stage `galaxy-publish` to publish playbooks to [`Ansible Galaxy`](https://galaxy.ansible.com/) on a tag pipeline.

### `Ansible Galaxy` - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/ansible.galaxy-publish.gitlab-ci.yml'
  ...

stages:
  ...
  - galaxy-publish
```

### `Ansible Galaxy` - variables

| CI/CD Protected/Masked Variables | Description                                                                                 | Type     |
| -------------------------------- | ------------------------------------------------------------------------------------------- | -------- |
| `GALAXY_TOKEN`                   | A [token or API key](https://docs.ansible.com/ansible/latest/cli/ansible-galaxy.html) value | Variable |

| Variables           | Description                    | Default            |
| ------------------- | ------------------------------ | ------------------ |
| `GALAXY_NAMESPACE`  | `Namespace` of the collection. | **Required value** |
| `GALAXY_COLLECTION` | Name of the collection.        | **Required value** |
| `IMAGE_ANSIBLE`     | Default `Ansible` Docker image | `jfxs/ansible`     |

## Lint

### Lint - details

A stage `lint` with 8 jobs:

* `lint:file`: Lint text files (utf8 encoded, Carriage Return at end of line, no trailing whitespaces at end of line, one new line at end of file)
* `lint:pre-commit`: Lint files with [`pre-commit`](https://pre-commit.com/)
* `lint:docker`: Lint `Dockerfile` with [`Hadolint`](https://github.com/hadolint/hadolint)
* `lint:link`: Check links with [Lychee](https://github.com/lycheeverse/lychee)
* `lint:markdown`: Lint Markdown files with [markdownlint-cli2](https://github.com/DavidAnson/markdownlint-cli2)
* `lint:shell`: Lint shell scripts with [`shellcheck`](https://www.shellcheck.net/)
* `lint:vale`: Lint English language [`vale`](https://vale.sh/)
* `lint:yaml`: Lint `yaml` file with [`yamllint`](https://github.com/adrienverge/yamllint)

### Lint - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/lint.gitlab-ci.yml'

variables:
  LINT_DOCKER: "true"
  LINT_SHELL: "true"
  LINT_SHELL_FILE: "files/*.sh"
  ...

stages:
  - lint
  ...
```

### Lint - variables

| Variables                       | Description                       | Default                             |
| ------------------------------- | --------------------------------- | ----------------------------------- |
| `LINT_FILE`                     | Lint text files job               | `"true"`                            |
| `LINT_FILE_ALLOW_FAILURE`       | Pipeline continues when job fails | `"true"`                            |
| `LINT_FILE_IMAGE`               | Docker image for job              | `jfxs/alpine-task`                  |
| `LINT_PRE_COMMIT`               | Lint files with pre-commit        | `"true"`                            |
| `LINT_PRE_COMMIT_ALLOW_FAILURE` | Pipeline continues when job fails | `"true"`                            |
| `LINT_PRE_COMMIT_IMAGE`         | Docker image for job              | `jfxs/pre-commit`                   |
| `LINT_DOCKER`                   | Lint `Dockerfile` file job        | `"false"`                           |
| `LINT_DOCKER_ALLOW_FAILURE`     | Pipeline continues when job fails | `"false"`                           |
| `LINT_DOCKER_FILE`              | File path to lint                 | `Dockerfile`                        |
| `LINT_DOCKER_IMAGE`             | Docker image for job              | `hadolint/hadolint:latest-alpine`   |
| `LINT_MARKDOWN`                 | Lint Markdown file job            | `"true"`                            |
| `LINT_MARKDOWN_ALLOW_FAILURE`   | Pipeline continues when job fails | `"true"`                            |
| `LINT_MARKDOWN_IMAGE`           | Docker image for job              | `davidanson/markdownlint-cli2`      |
| `LINT_MARKDOWN_GLOB`            | Files to lint                     | `'"**/*.md"'`                       |
| `LINT_SHELL`                    | Lint shell files job              | `"false"`                           |
| `LINT_SHELL_ALLOW_FAILURE`      | Pipeline continues when job fails | `"false"`                           |
| `LINT_SHELL_FILE`               | Shell file path                   | `"*.sh"`                            |
| `LINT_SHELL_IMAGE`              | Docker image for job              | `koalaman/shellcheck-alpine:stable` |
| `LINT_VALE`                     | Lint English language job         | `"false"`                           |
| `LINT_VALE_ALLOW_FAILURE`       | Pipeline continues when job fails | `"true"`                            |
| `LINT_VALE_FILE`                | Vale file path                    | `"*.md"`                            |
| `LINT_VALE_IMAGE`               | Docker image for job              | `jdkato/vale`                       |
| `LINT_VALE_STYLES_DIR`          | Vale styles directory             | `styles`                            |
| `LINT_YAML`                     | Lint YAML files                   | `"true"`                            |
| `LINT_YAML_ALLOW_FAILURE`       | Pipeline continues when job fails | `"true"`                            |
| `LINT_YAML_IMAGE`               | Docker image for job              | `jfxs/ansible`                      |

## Lint python

### Lint python - details

A stage `lint` with 4 jobs:

* `lint:py-black`: The uncompromising Python code formatter [`Black`](https://black.readthedocs.io/en/stable/)
* `lint:py-mypy`: Static type checker with [`Mypy`](https://mypy.readthedocs.io/en/stable/index.html)
* `lint:py-ruff`: Lint with [Ruff](https://docs.astral.sh/ruff/), a fast Python linter, written in Rust
* `lint:py-flake8`: [Flake8](https://flake8.pycqa.org) linter

### Lint python - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/lint.python-rye.gitlab-ci.yml'

variables:
  LINT_PY_BLACK_SRC: "my_src/ my_tests/"
  LINT_PY_MYPY: "false"
  ...

stages:
  - lint
  ...
```

### Lint python - variables

| Variables                      | Description                       | Default                        |
| ------------------------------ | --------------------------------- | ------------------------------ |
| `LINT_PY_BLACK`                | Black code formatter job          | `"true"`                       |
| `LINT_PY_BLACK_ALLOW_FAILURE`  | Pipeline continues when job fails | `true`                         |
| `LINT_PY_BLACK_IMAGE`          | Docker image for job              | `pyfound/black:latest_release` |
| `LINT_PY_BLACK_SRC`            | Directories to format             | `"src/ tests/"`                |
| `LINT_PY_MYPY`                 | `Mypy` Static type checker job    | `"true"`                       |
| `LINT_PY_MYPY_ALLOW_FAILURE`   | Pipeline continues when job fails | `"true"`                       |
| `LINT_PY_MYPY_IMAGE`           | Docker image for job              | `jfxs/rye`                     |
| `LINT_PY_MYPY_FLAG`            | `Mypy` CLI options                | `"--strict"`                   |
| `LINT_PY_MYPY_SRC`             | Directories to check              | `"src/ tests/"`                |
| `LINT_PY_RUFF`                 | Lint with Ruff                    | `"true"`                       |
| `LINT_PY_RUFF_ALLOW_FAILURE`   | Pipeline continues when job fails | `"true"`                       |
| `LINT_PY_RUFF_IMAGE`           | Docker image for job              | `jfxs/rye`                     |
| `LINT_PY_RUFF_SRC`             | Directories to check              | `"src/ tests/"`                |
| `LINT_PY_FLAKE8`               | Lint with Flake8                  | `"true"`                       |
| `LINT_PY_FLAKE8_ALLOW_FAILURE` | Pipeline continues when job fails | `"true"`                       |
| `LINT_PY_FLAKE8_IMAGE`         | Docker image for job              | `jfxs/rye`                     |
| `LINT_PY_FLAKE8_SRC`           | Directories to check              | `"src/ tests/"`                |

## Image factory

### Image factory - details

4 stages:

* `build-image`: Build image for amd64 and arm64 architecture, push it in the GitLab registry. Set `DOCKER_BUILD_VERSION` required variable in `before_script` step.
* `test-image`: The stage has the `sanity-test:version` job. Set `DOCKER_VERSION_CLI` required variable in `before_script` step.
* `trigger-publish-image`: Define the `DOCKER_SBOM_SUBSET` RE variable to trigger the publishing of an image.
* `publish-image`: Publish to `Dockerhub` and `Quay.io`

### Image factory - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/image-factory.gitlab-ci.yml'

variables:
  DOCKER_REGISTRY_IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
  DOCKER_DOCKERHUB_IMAGE_REPOSITORY: index.docker.io/jfxs/test-ci
  DOCKER_DOCKERHUB_RM_URL: jfxs/test-ci
  DOCKER_QUAY_IMAGE_REPOSITORY: quay.io/ifxs/test-ci
  DOCKER_SBOM_SUBSET: "^github.com/go-task/task/v3 |^ca-certificates |^curl |^file |^git |^jq |^ncurses "
  DOCKER_TASKFILE_VERSION: .Taskfile-version.yml
  DOCKER_TEST_VERSION_DIND: "true"
  ...

stages:
  ...
  - build-image
  - test-image
  - trigger-publish-image
  - publish-image
  ...

docker:build:
  before_script:
    - if [ ! -f "$TASKFILE_VERSION" ]; then wget -q -O "$TASKFILE_VERSION" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/version.yml; fi
    - DOCKER_BUILD_VERSION=$(task --taskfile "$TASKFILE_VERSION" get-latest-github REPO=go-task/task)

sanity-test:version:
  before_script:
    - if [ ! -f "$TASKFILE_VERSION" ]; then wget -q -O "$TASKFILE_VERSION" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/version.yml; fi
    - DOCKER_VERSION_CLI=$(task --taskfile "$TASKFILE_VERSION" get-docker-cli-version IMG="$DOCKER_REGISTRY_IMAGE_TAG" CMD="task --version")

# or if DOCKER_TEST_VERSION_DIND = false
sanity-test:version-not-dind:
  script:
    - version_cli=$(cd /home/node && npx semantic-release --version)
    - echo "Version from cli -> $version_cli <"
    - if [ "$DOCKER_MANIFEST_VERSION" = "$version_cli" ]; then printf "\033[0;32m[OK]\033[0m Versions from CLI and manifest are equal > %s <-> %s <\n" "$DOCKER_MANIFEST_VERSION" "$version_cli"; else printf "\033[0;31m[FAILED] Versions from CLI and manifest are different > %s <-> %s <\n" "$DOCKER_MANIFEST_VERSION" "$version_cli" && exit 1; fi
...
```

### Image factory - variables

| CI/CD Protected/Masked Variables | Description                        | Type     |
| -------------------------------- | ---------------------------------- | -------- |
| `DOCKERHUB_USER`                 | `Dockerhub` user                   | Variable |
| `DOCKERHUB_PASSWORD`             | `Dockerhub` token                  | Variable |
| `COSIGN_PASSWORD`                | Cosign password of the private key | Variable |
| `COSIGN_PUBLIC_KEY`              | Public key                         | File     |
| `COSIGN_PRIVATE_KEY`             | Private key                        | File     |
| `QUAY_USER`                      | `Quay.io` user                     | Variable |
| `QUAY_PASSWORD`                  | `Quay.io` password                 | Variable |
| `APIKEY__QUAY_IO`                | `Quay.io` API token                | Variable |

| Variables                           | Description                                                                               | Default                                                       |
| ----------------------------------- | ----------------------------------------------------------------------------------------- | ------------------------------------------------------------- |
| `DOCKER_DOCKERHUB_IMAGE_REPOSITORY` | `Dockerhub` image repository. Ex: `index.docker.io/jfxs/test-ci`                          | **Required value**                                            |
| `DOCKER_DOCKERHUB_RM_URL`           | Update `readme` in `Dockerhub` repository. Ex: `jfxs/test-ci`                             | **Required value**                                            |
| `DOCKER_QUAY_IMAGE_REPOSITORY`      | Quay.io image repository. Ex: `quay.io/ifxs/test-ci`                                      | **Required value**                                            |
| `DOCKER_SBOM_SUBSET`                | Regular expression to trigger the publication. Ex: `"^github.com/go-task/task/v3 \|^jq "` | **Required value**                                            |
| `DOCKER_IMAGE_FACTORY`              | Docker image with `cosign jq skopeo syft`                                                 | `jfxs/image-factory:latest`                                   |
| `DOCKER_REGISTRY_IMAGE_TAG`         | `Gitlab` registry image tag                                                               | `$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA` |
| `DOCKER_SERVICE_DOCKER_TAG`         | Docker service tag                                                                        | `stable-dind`                                                 |
| `DOCKER_TASKFILE_DOCKER`            | Docker Task file path                                                                     | `.Taskfile-docker.yml`                                        |
| `DOCKER_TASKFILE_SBOM`              | Software Bill of Materials (SBOM) Task file path                                          | `.Taskfile-sbom.yml`                                          |
| `DOCKER_TASKFILE_VERSION`           | Version Task file path                                                                    | `.Taskfile-version.yml`                                       |
| `DOCKER_TEST_VERSION_DIND`          | Check version of image with Docker in Docker or not                                       | `"true"`                                                      |

## `GitLab` release

### `GitLab` release - details

A stage `gitlab-release` to publish a GitLab release with [Semantic Release](https://github.com/semantic-release/semantic-release).

### `GitLab` release - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/gitlab-release.gitlab-ci.yml'
  ...

stages:
  ...
  - gitlab-release
```

### `GitLab` release - variables

| CI/CD Protected/Masked Variables | Description                                                                                          | Type     |
| -------------------------------- | ---------------------------------------------------------------------------------------------------- | -------- |
| `GITLAB_TOKEN`                   | A [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) value | Variable |

| Variables               | Description                    | Default                                   |
| ----------------------- | ------------------------------ | ----------------------------------------- |
| `GL_RELEASE_IMAGE_NODE` | Default Semantic release image | `jfxs/commitizen-semantic-release:latest` |

## Python release with `Rye`

### Python release - details

Two stages `pypi-release` and `pages-release` to publish a GitLab release to [PyPI](https://pypi.org/) and the documentation to `GitLab` pages with [Rye](https://rye-up.com/) on a tag pipeline.

### Python release - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/python.rye-publish.gitlab-ci.yml'
  ...

stages:
  ...
  - pypi-release
  - pages-release
```

### Python release - variables

| CI/CD Protected/Masked Variables | Description                                      | Type     |
| -------------------------------- | ------------------------------------------------ | -------- |
| `PYPI_TOKEN`                     | A [token](https://pypi.org/help/#apitoken) value | Variable |

| Variables           | Description                       | Default                  |
| ------------------- | --------------------------------- | ------------------------ |
| `IMAGE_PYTHON`      | Default python Docker image       | `jfxs/rye`               |
| `DOC_PUBLISH_PAGES` | Pages  job                        | `"true"`                 |
| `TASK_SET_VERSION`  | Name of the task to set a version | `00:project-set-version` |

## Robot framework

### Robot framework - details

Two jobs to lint Robot Framework files with [`Robotidy`](https://github.com/MarketSquare/robotframework-tidy) and [`Robocop`](https://github.com/MarketSquare/robotframework-robocop).

### Robot framework - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/robotframework.gitlab-ci.yml'
  ...

stages:
  - lint
  ...
```

### Robot framework - variables

| Variables                     | Description                                                                     | Default  |
| ----------------------------- | ------------------------------------------------------------------------------- | -------- |
| `LINT_ROBOTIDY_DIR`           | Robot Framework files path. Required to run `lint:robotidy` job. Ex: `tests/RF` | `""`     |
| `LINT_ROBOTIDY_ALLOW_FAILURE` | Pipeline continues when job fails                                               | `"true"` |
| `LINT_ROBOCOP_DIR`            | Robot Framework files path. Required to run `lint:robocop` job. Ex: `tests/RF`  | `""`     |
| `LINT_ROBOCOP_ALLOW_FAILURE`  | Pipeline continues when job fails                                               | `"true"` |

## Robot framework pages

### Robot framework pages - details

A stage to publish Robot Framework report to [`Gitlab Pages`](https://docs.gitlab.com/ee/user/project/pages/).

### Robot framework pages - installation

Example of a `.gitlab-ci.yml` configuration:

```shell
include:
  - remote: 'https://gitlab.com/op_so/projects/gitlab-ci-templates/-/raw/main/templates/robotframework-pages.gitlab-ci.yml'
  ...

stages:
  ...
  - rebot-tests
  - robot-framework-pages

rf-test:
  ...
  script:
  - robot --log none --report none --outputdir "$REBOT_INPUT_DIR" --output output1.xml tests/RF
  ...
  artifacts:
    when: always
    paths:
      - reports
    expire_in: 1 week

  ...

rebot-tests:
  dependencies:
    - rf-test
```

### Robot framework pages - variables

| Variables         | Description                                                                        | Default   |
| ----------------- | ---------------------------------------------------------------------------------- | --------- |
| `REBOT_INPUT_DIR` | Output `*.xml` file results of Robot Framework should be publish in this directory | `reports` |

## Authors

<!-- vale off -->
* **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
<!-- vale on -->

## License

<!-- vale off -->
This program is free software: you can redistribute it and/or modify it under the terms of the MIT License (MIT). See the [LICENSE](https://opensource.org/licenses/MIT) for details.
<!-- vale on -->
