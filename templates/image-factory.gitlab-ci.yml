---
variables:
  DOCKER_DOCKERFILE: Dockerfile
  DOCKER_IMAGE_FACTORY: jfxs/image-factory:latest
  DOCKER_REGISTRY_IMAGE_TAG: $CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG:$CI_COMMIT_SHORT_SHA
  DOCKER_SERVICE_DOCKER_TAG: 25-dind
  DOCKER_TASKFILE_DOCKER: .Taskfile-docker.yml
  DOCKER_TASKFILE_SBOM: .Taskfile-sbom.yml
  DOCKER_TASKFILE_VERSION: .Taskfile-version.yml
  DOCKER_TEST_VERSION_DIND: "true"

.snippets:
  prerequisites-task:
    - if [ ! -f "$DOCKER_TASKFILE_DOCKER" ]; then wget -q -O "$DOCKER_TASKFILE_DOCKER" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/docker.yml; fi
    - if [ ! -f "$DOCKER_TASKFILE_SBOM" ]; then wget -q -O "$DOCKER_TASKFILE_SBOM" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/sbom.yml; fi
    - if [ ! -f "$DOCKER_TASKFILE_VERSION" ]; then wget -q -O "$DOCKER_TASKFILE_VERSION" https://gitlab.com/op_so/task/task-templates/-/raw/main/Taskfile.d/version.yml; fi
  # Check digest and signature of new image
  check-new-image:
    - digest_registry=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" get-digest IMG="$DOCKER_REGISTRY_IMAGE_TAG")
    - digest_ci=$(cat image-digest.txt)
    - if [ "$digest_registry" = "$digest_ci" ]; then printf "\033[0;32m[OK]\033[0m Digest from registry and artifacts are equal.\n"; else printf "\033[0;31m[FAILED] Versions from registry and artifacts are different\n > %s < \n > %s <\n" "$digest_registry" "$digest_ci" && exit 1; fi
    - cosign verify --insecure-ignore-tlog=true --key "$COSIGN_PUBLIC_KEY" "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci"

docker:build:
  image: $DOCKER_IMAGE_FACTORY
  stage: build-image
  services:
    - docker:$DOCKER_SERVICE_DOCKER_TAG
  variables:
    DOCKER_HOST: tcp://docker:2375
  script:
    - !reference [.snippets, prerequisites-task]
    - if [ -z "$DOCKER_BUILD_VERSION" ]; then printf "\033[0;31m[ERROR] DOCKER_BUILD_VERSION variable is empty!\033[0m\n" && exit 1; fi
    - printf "\033[0;32m[OK] DOCKER_BUILD_VERSION \"%s\". \033[0m\n" "$DOCKER_BUILD_VERSION"
    # Build
    - task --taskfile "$DOCKER_TASKFILE_DOCKER" build-push-multiarch DOCKER_USER="$CI_REGISTRY_USER" DOCKER_PASS="$CI_REGISTRY_PASSWORD" TAG="$DOCKER_REGISTRY_IMAGE_TAG" VERSION="$DOCKER_BUILD_VERSION" VCS_REF="$CI_COMMIT_SHORT_SHA" REGISTRY="$CI_REGISTRY"
    - digest=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" get-digest IMG="$DOCKER_REGISTRY_IMAGE_TAG")
    - echo "$digest" > image-digest.txt
    # Sign and SBOM
    - echo -n "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
    - if [ -z "$COSIGN_PASSWORD" ]; then printf "\033[0;31m[ERROR] COSIGN_PASSWORD variable is empty!\033[0m\n" && exit 1; fi
    # - cosign sign --yes --key "$COSIGN_PRIVATE_KEY" "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest"
    - cosign sign --tlog-upload=false --key "$COSIGN_PRIVATE_KEY" "$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest"
    - task --taskfile "$DOCKER_TASKFILE_SBOM" attach-sbom-attest I="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest" K="$COSIGN_PRIVATE_KEY"
  after_script:
    - docker logout "$CI_REGISTRY"
  artifacts:
    paths:
      - image-digest.txt
    expire_in: 1 week

sanity-test:get-manifest-version:
  image: $DOCKER_IMAGE_FACTORY
  stage: test-image
  script:
    - !reference [.snippets, prerequisites-task]
    - version_manifest=$(task --taskfile "$DOCKER_TASKFILE_VERSION" get-docker-manifest-version IMG="$DOCKER_REGISTRY_IMAGE_TAG")
    - if [ -z "$version_manifest" ]; then printf "\033[0;31m[ERROR] Version manifest variable is empty!\033[0m\n" && exit 1; fi
    - echo "Version from manifest -> $version_manifest <"
    - echo "$version_manifest" > image-manifest-version.txt
  artifacts:
    paths:
      - image-manifest-version.txt
    expire_in: 1 week

sanity-test:version:
  image: $DOCKER_IMAGE_FACTORY
  stage: test-image
  services:
    - docker:$DOCKER_SERVICE_DOCKER_TAG
  script:
    - if [ -z "$DOCKER_VERSION_CLI" ]; then printf "\033[0;31m[ERROR] DOCKER_VERSION_CLI variable is empty!\033[0m\n" && exit 1; fi
    - echo "Version from CLI -> $DOCKER_VERSION_CLI <"
    - version_manifest=$(cat image-manifest-version.txt)
    - echo "Version from manifest -> $version_manifest <"
    - if [ "$DOCKER_VERSION_CLI" = "$version_manifest" ]; then printf "\033[0;32m[OK]\033[0m Versions from CLI and manifest are equal > %s <-> %s <\n" "$DOCKER_VERSION_CLI" "$version_manifest"; else printf "\033[0;31m[FAILED] Versions from CLI and manifest are different > %s <-> %s <\n" "$DOCKER_VERSION_CLI" "$version_manifest" && exit 1; fi
  dependencies:
    - sanity-test:get-manifest-version
  needs: ["sanity-test:get-manifest-version"]
  rules:
    - if: $DOCKER_TEST_VERSION_DIND == "true"

sanity-test:version-not-dind:
  image: $DOCKER_REGISTRY_IMAGE_TAG
  stage: test-image
  before_script:
    - DOCKER_MANIFEST_VERSION=$(cat image-manifest-version.txt)
  script:
    - printf "\033[0;31m[FAILED] script keyword must be surcharged!" && exit 1
  dependencies:
    - sanity-test:get-manifest-version
  needs: ["sanity-test:get-manifest-version"]
  rules:
    - if: $DOCKER_TEST_VERSION_DIND != "true"

trigger:publish:
  image: $DOCKER_IMAGE_FACTORY
  stage: trigger-publish-image
  variables:
    PUBLISH: 0
  script:
    - !reference [.snippets, prerequisites-task]
    - !reference [.snippets, check-new-image]
    # Verify latest image signature, if failed -> publish
    - if ! cosign verify --insecure-ignore-tlog=true --key "$COSIGN_PUBLIC_KEY" "$DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest"; then echo "Signature verification failed for $DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest -> Publish" && PUBLISH=1; fi
    # Check latest layer of base image for latest image and new image, if different -> publish
    # Get number of layers of base image
    - base_image=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" get-base-image FILE="$DOCKER_DOCKERFILE") && echo "Base image> $base_image <"
    - layers_count=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" count-layer IMG="$base_image") && echo "Layer count> $layers_count <"
    - layer_id="$(( layers_count - 1 ))" && echo "Layer [ ${layer_id} ] "
    - if ! base_latest=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" get-digest-layer-n NUM="$layer_id" IMG="$DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest"); then echo "Get base image failed for $DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest -> Publish" && base_latest=""; fi
    - base_new=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" get-digest-layer-n NUM="$layer_id" IMG="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci")
    - echo "Base image latest -> $base_latest" && echo "Base image new    -> $base_new"
    - if [ "$base_latest" != "$base_new" ]; then echo "Base image are different -> Publish" && PUBLISH=1; fi
    # Check metadata version for latest image and new image, if different -> publish
    - if ! version_latest=$(task --taskfile "$DOCKER_TASKFILE_VERSION" get-docker-manifest-version IMG="$DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest"); then echo "Get version metadata failed for $DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest -> Publish" && version_latest=""; fi
    - version_new=$(task --taskfile "$DOCKER_TASKFILE_VERSION" get-docker-manifest-version IMG="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci")
    - echo "Version metadata latest -> $version_latest" && echo "Version metadata new    -> $version_new"
    - if [ "$version_latest" != "$version_new" ]; then echo "Version metadata are different -> Publish" && PUBLISH=1; fi
    # Check SBOM subset for latest image and new image, if different -> publish
    - if ! sbom_latest=$(task --taskfile "$DOCKER_TASKFILE_SBOM" get-sbom-attest IMG="$DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest" KEY="$COSIGN_PUBLIC_KEY" OUTPUT=sbom_latest.txt FORMAT=table); then echo "Get SBOM attestation failed for $DOCKER_DOCKERHUB_IMAGE_REPOSITORY:latest -> Publish" && touch sbom_latest.txt; fi
    - task --taskfile "$DOCKER_TASKFILE_SBOM" get-sbom-attest IMG="$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" KEY="$COSIGN_PUBLIC_KEY" OUTPUT=sbom_new.txt FORMAT=table
    - echo "Subset of SBOM > $DOCKER_SBOM_SUBSET <"
    - cat sbom_new.txt
    - diff_latest_new=$(task --taskfile "$DOCKER_TASKFILE_SBOM" diff-sbom INPUT1=sbom_latest.txt INPUT2=sbom_new.txt GREP="$DOCKER_SBOM_SUBSET")
    - if [ "$diff_latest_new" = "1" ]; then echo "SBOM subsets are different -> Publish" && PUBLISH=1; fi
    # Prepare to publish
    - echo "PUBLISH=$PUBLISH" >> trigger.env
    - echo "VERSION_NEW=$version_new" >> trigger.env
    - version_major_new=$(task --taskfile "$DOCKER_TASKFILE_VERSION" get-major-version VERSION="$version_new") && echo "TAG_MAJOR_NEW=$version_major_new" >> trigger.env
    - version_major_minor_new=$(task --taskfile "$DOCKER_TASKFILE_VERSION" get-major-minor-version VERSION="$version_new") && echo "TAG_MAJOR_MINOR_NEW=$version_major_minor_new" >> trigger.env
    - if [ "$PUBLISH" = "1" ]; then index_new=$(task --taskfile "$DOCKER_TASKFILE_DOCKER" get-next-tag-index IMG="$DOCKER_DOCKERHUB_IMAGE_REPOSITORY" VERSION="$version_new"); fi
    - if [ "$PUBLISH" = "1" ] && [ -z "$index_new" ]; then printf "\033[0;31m[ERROR] Next tag index is not defined!\n" && exit 1; fi
    - if [ "$PUBLISH" = "1" ]; then echo "TAG_FULL_NEW=$version_new-$index_new" >> trigger.env && echo "Publish new image with tag $version_new-$index_new"; fi
  dependencies:
    - docker:build
  artifacts:
    paths:
      - sbom_new.txt
    reports:
      dotenv: trigger.env

publish:dockerhub:
  image: $DOCKER_IMAGE_FACTORY
  stage: publish-image
  services:
    - docker:$DOCKER_SERVICE_DOCKER_TAG
  variables:
    REGISTRY_USER: $DOCKERHUB_USER
    REGISTRY_PASS: $DOCKERHUB_PASSWORD
    REGISTRY_URL: $DOCKER_DOCKERHUB_IMAGE_REPOSITORY
    REGISTRY_RM_URL: $DOCKER_DOCKERHUB_RM_URL
  before_script:
    - if [ "$PUBLISH" = "1" ]; then echo -n "$REGISTRY_PASS" | docker login -u "$REGISTRY_USER" --password-stdin; fi
  script:
    - !reference [.snippets, prerequisites-task]
    - !reference [.snippets, check-new-image]
    # Update README
    - sed -i'.bu' "s=--VERSION--=$TAG_FULL_NEW, $TAG_MAJOR_MINOR_NEW, $TAG_MAJOR_NEW=g" README.md
    - task --taskfile "$DOCKER_TASKFILE_SBOM" get-sbom-subset INPUT=sbom_new.txt OUTPUT=sbom_subset.txt GREP="$DOCKER_SBOM_SUBSET"
    - task --taskfile "$DOCKER_TASKFILE_SBOM" set-sbom-in-file SBOM=sbom_subset.txt
    # Publish, sign, sbom full version
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:$TAG_FULL_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then cosign sign --tlog-upload=false --key "$COSIGN_PRIVATE_KEY" "$REGISTRY_URL:$TAG_FULL_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then task --taskfile "$DOCKER_TASKFILE_SBOM" attach-sbom-attest I="$REGISTRY_URL:$TAG_FULL_NEW" K="$COSIGN_PRIVATE_KEY"; fi
    # Publish major and major-minor latest tag
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:$TAG_MAJOR_MINOR_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:$TAG_MAJOR_NEW"; fi
    - if [ "$PUBLISH" = "1" ]; then skopeo copy --multi-arch all --dest-creds $REGISTRY_USER:$REGISTRY_PASS "docker://$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG@$digest_ci" "docker://$REGISTRY_URL:latest"; fi
    # Publish README
    - if [ "$PUBLISH" = "1" ]; then docker pushrm "$REGISTRY_RM_URL"; fi
  after_script:
    - docker logout
  dependencies:
    - docker:build
    - trigger:publish
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

publish:quay:
  extends: publish:dockerhub
  variables:
    REGISTRY_USER: $QUAY_USER
    REGISTRY_PASS: $QUAY_PASSWORD
    REGISTRY_URL: $DOCKER_QUAY_IMAGE_REPOSITORY
    REGISTRY_RM_URL: $DOCKER_QUAY_IMAGE_REPOSITORY
  before_script:
    - if [ "$PUBLISH" = "1" ]; then echo -n "$REGISTRY_PASS" | docker login -u "$REGISTRY_USER" --password-stdin quay.io; fi
  after_script:
    - docker logout quay.io
  needs:
    - docker:build
    - trigger:publish
    - publish:dockerhub
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $DOCKER_QUAY_IMAGE_REPOSITORY
